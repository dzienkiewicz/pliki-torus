
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <errno.h>

#define MESSAGE_SIZE 4000


int idKolejki;
int rozmiarwiadomosci;

struct Wiadomosc
{
    long int odbiorca;
    long int nadawca;
    char tresc[MESSAGE_SIZE];
};

void globals();
void dolaczDoKolejki();
void wyswietlWiadomosc(struct Wiadomosc *wiadomosc);

void* watekWysylania();
void* watekOdbierania();

void globals()
{
    rozmiarwiadomosci = MESSAGE_SIZE*sizeof(char)+(sizeof(long int));
}

void dolaczDoKolejki()
{
    idKolejki = msgget(2020, 0660|IPC_CREAT);
    if(idKolejki == -1)
    {
        printf("KLIENT: Blad dolaczenia do kolejki\n");
        exit(1);
    }

    printf("KLIENT: Dolaczono do kolejki o id: %d\n",idKolejki);
}

void wyswietlWiadomosc(struct Wiadomosc *wiadomosc)
{
    printf("KLIENT: Nadawca: %ld\n",wiadomosc->nadawca);
    printf("KLIENT: Odbiorca: %ld\n",wiadomosc->odbiorca);
    printf("KLIENT: Tresc: %s\n\n",wiadomosc->tresc);
}

void odbierzWiadomosc(struct Wiadomosc *wiadomosc)
{
    if(msgrcv( idKolejki, wiadomosc, 4008, (long int) getpid(), 0 ) == -1)
    {
        if(errno == EIDRM)
        {
            printf("KLIENT: Blad podczas odbierania komunikatu, kolejka usunieta\n");
            exit(1);
        }

        if(errno == EINTR)
        {
            printf("KLIENT: Blad podczas odbierania komunikatu,wznawiam\n");
            return odbierzWiadomosc (wiadomosc);
        }

        if(errno == EAGAIN)
        {
            printf("KLIENT: Blad podczas odbierania komunikatu, kolejka pelna!\n");
            return;
        }

        printf("KLIENT: komunikat zbyt duzy zostanie zignorowany\n");
        return;
    }
}

void sendMessage(char* wiad)
{
    struct Wiadomosc wiadomosc;

    wiadomosc.odbiorca = 1;
    wiadomosc.nadawca = getpid();
    strcpy(wiadomosc.tresc,wiad);

    if(msgsnd(idKolejki, &wiadomosc, 4008, 0) == -1)
    {
        if(errno == EIDRM)
        {
            printf("KLIENT: Blad podczas wysylania komunikatu\n");
            exit(1);
        }

        if(errno == EINTR)
        {
            printf("KLIENT: Blad podczas wysylania komunikatu,wznawiam\n");
            return sendMessage(wiad);
        }

        if(errno == EAGAIN)
        {
            printf("KLIENT: Blad podczas wysylania komunikatu, kolejka pelna!\n"); // msg_qbytes limit
            return;
        }

        printf("KLIENT: Nie udalo sie wyslac wiadomosci\n");
        exit(1);
    }

}

void* watekWysylania()
{
    int i;
    char* wiad = malloc(MESSAGE_SIZE*sizeof(char));

    i=0;

    while(1)
    {
        printf("KLIENT: Wysylam wiadomosc nr: %d ...\n",i++);
        printf("KLIENT: Podaj tresc\n");
        scanf("%5s",wiad);
        printf("\n");
        sendMessage(wiad);

        printf("Wyslano\n");
        //sleep(1);
    }

    free(wiad);

    pthread_exit(0);
}

void* watekOdbierania()
{
    int i;
    struct Wiadomosc wiadomosc;

    while(1)
    {
        odbierzWiadomosc (&wiadomosc);
        wyswietlWiadomosc (&wiadomosc);
        sleep(1);
    }

    pthread_exit(0);
}

void createThread(pthread_t* threadId, void* threadFunction)
{
    int threadError;

    threadError = pthread_create(threadId, NULL, threadFunction, NULL);
    if (threadError != 0)
        printf("\nKLIENT: Nie udalo sie stworzyc watku :[%s]\n", strerror(threadError));
    else
        printf("\nKLIENT: Udalo sie stworzyc watkek\n");
}

void waitThread(pthread_t threadId)
{
    int threadError;
    int** threadStatus;

    threadStatus = malloc(sizeof(*threadStatus));
    *threadStatus = malloc(sizeof(**threadStatus));

    threadError = pthread_join(threadId, (void**) threadStatus);
    if (threadError != 0)
    {
        printf("\nKLIENT: Nie udalo sie dolaczyc watku :[%s]", strerror(threadError));
        printf("\nKLIENT: Nie udalo sie dolaczyc watku :[%d]", ** (int**)threadStatus);
    }
    else
        printf("\nKLIENT: Udalo sie dolaczyc watek\n");

    free(*threadStatus);
    free(threadStatus);
}

int main()
{
    pthread_t senderThreadId;
    pthread_t receiverThreadId;

    globals ();

    dolaczDoKolejki ();

    createThread(&senderThreadId, &watekWysylania);
    createThread(&receiverThreadId, &watekOdbierania);

    waitThread(senderThreadId);
    waitThread(receiverThreadId);

    return 0;
}
