#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>

#define MESSAGE_SIZE 10

int idKolejki;
int rozmiarwiadomosci;

struct Wiadomosc
{
    long int odbiorca;
    long int nadawca;
    char tresc[MESSAGE_SIZE];
};


void start();
void sygnatura(int signal);
void stworzKolejkeWiadomosci();
void usunKolejkeWiadomosci();
void wyswietlWiadomosc(struct Wiadomosc *wiadomosc);
int odbierzWiadomosc(struct Wiadomosc *wiadomosc);
void wyslijWiadomosc(struct Wiadomosc *wiadomosc);

void start()
{
    signal (SIGINT, sygnatura);
    rozmiarwiadomosci = MESSAGE_SIZE*sizeof(char)+sizeof(long int);
}

void sygnatura(int signal)
{
    printf("\nSerwer zostal wylaczony Ctrl+C \n");
    usunKolejkeWiadomosci ();
    exit(1);
}

void stworzKolejkeWiadomosci()
{
    idKolejki = msgget(1111, 0600|IPC_CREAT|IPC_EXCL);
    if(idKolejki == -1)
    {
        printf("Blad tworzenia kolejki!\n");
        exit(1);
    }

    printf("Utworzona kolejka o id: %d\n",idKolejki);
}

void usunKolejkeWiadomosci()
{

    if(msgctl(idKolejki, IPC_RMID, NULL) == -1)
    {
        printf("SERWER: blad usuwania kolejki");
        exit(1);
    }

    printf("SERWER: usunieta kolejka\n");
}

void wyswietlWiadomosc(struct Wiadomosc *wiadomosc)
{
    int i;

    printf("SERWER: nadawca: %ld\n",wiadomosc->nadawca);
    printf("SERWER: odbiorca: %ld\n",wiadomosc->odbiorca);
    printf("SERWER: tresc: ");

    for(i=0;i<MESSAGE_SIZE;i++)
    {
        if(wiadomosc->tresc[i] == '\0')
            break;
        printf("%c",wiadomosc->tresc[i]);
    }

    printf("\n\n");

}

int odbierzWiadomosc(struct Wiadomosc *wiadomosc)
{
    if(msgrcv( idKolejki, wiadomosc, rozmiarwiadomosci, 1, 0) == -1)
    {
        if(errno == EIDRM)
        {
            printf("Blad podczas odbierania komunikatu, kolejka zostala skasowana\n");
            exit(1);
        }

        if(errno == EINTR)
        {
            printf("Blad podczas odbierania komunikatu, wznowienie procesu\n");
            return odbierzWiadomosc (wiadomosc);
        }

        if(errno == EAGAIN)
        {
            printf("Blad podczas odbierania komunikatu, kolejka pelna!\n");
            return 1;
        }

        printf("Blad podczas odbierania komunikatu, komunikat zbyt duzy zostanie zignorowany\n");
        return(1);
    }

    return 0;

}

void wyslijWiadomosc(struct Wiadomosc *wiadomosc)
{
    int i;
    wiadomosc->odbiorca = wiadomosc->nadawca;
    wiadomosc->nadawca = 1;

    for(i=0;i<strlen(wiadomosc->tresc);i++)
    {
        wiadomosc->tresc[i] = toupper(wiadomosc->tresc[i]);
    }

    if(msgsnd( idKolejki, wiadomosc, rozmiarwiadomosci, 0) == -1)
    {
        if(errno == EIDRM)
        {
            printf("Blad podczas wysylania komunikatu, kolejka zostala skasowana\n");
            exit(1);
        }

        if(errno == EINTR)
        {
            printf("Blad podczas wysylania komunikatu, wznowienie procesu\n");
            wiadomosc->nadawca = wiadomosc->odbiorca;
            return wyslijWiadomosc (wiadomosc);
        }

        if(errno == EAGAIN)
        {
            printf("Blad podczas wysylania komunikatu, kolejka pelna!\n");
            return;
        }

        printf("Blad podczas wysylania komunikatu, pelna kolejka?\n");
        exit(1);
    }
}


int main()
{
    char c;
    struct Wiadomosc wiadomosc;

    start ();

    stworzKolejkeWiadomosci ();


    while(1)
    {
        printf("SERWER: czekam na wiadomosc\n");
        if(odbierzWiadomosc (&wiadomosc) == 1)
            continue;//dlugosc wiadomosci wieksza niz okreslony max, ignoruje ta wiadomosc
        printf("SERWER: odebrana wiadomosc\n");

        wyswietlWiadomosc (&wiadomosc);

        printf("SERWER: wysylam odpowiedz...\n");
        wyslijWiadomosc (&wiadomosc);
        printf("SERWER: odpowiedz wyslana\n");
    }

    usunKolejkeWiadomosci ();

    return 0;
}