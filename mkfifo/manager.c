#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

void utworzProducenta(int n);
void utworzKonsumenta(int n);
void utworzFifo();
char path[]="/tmp/Z2";
void utworzProducenta(int n)
{
  pid_t id;
  int i,execEr;

  for(i=0; i<n; i++)
  {
    id = fork();
    if(id == -1)
    {
      printf("PRODUCENT: Nieprawidlowe wywolanie fork()\n");
      exit(1);
    }
    if(id == 0)//jezeli proces jest procesem potomnym
    {
      execEr = execl("./producent.out","producent.out",NULL);
      if(execEr == -1)
      {
        printf("PRODUCENT: Blad wywolania execl\n");
        exit(2);
      }      
      exit(0);
    }
  }
}

void utworzKonsumenta(int n)
{
  pid_t id;
  int i,execEr;

  for(i=0; i<n; i++)
  {
    id = fork();
    if(id == -1)
    {
      printf("KONSUMENT: Nieprawidlowe wywolanie fork()\n");
      exit(1);
    }
    if(id == 0)
    {
      execEr = execl("./konsument.out","konsument.out",NULL);
      if(execEr == -1)
      {
        printf("KONSUMENT: Blad wywolania execl\n");
        exit(2);
      }

      exit(0);
    }
  }
  printf("Dla poprawnego wywolania dodalem sekundowe opoznienie!\n");
}

void utworzFifo()
{
  if(mkfifo(path, 0666 | 0)==-1)
  {
    if(errno == EEXIST)
    {
      printf("Fifo istnieje, usuwam i tworze nowe!\n");
      unlink(path);
      utworzFifo ();
      return;
    }
    else
    {
      printf("Blad tworzenia fifo\n");
      exit(1);
    }
    
  }
  else
  {
    printf("!!Stworzono kolejke fifo!!\n");
  }
}

int main(int argc, char* argv[])
{
  int i,cpid,status,iloscProducentow,iloscKonsumentow;
  system("./sprzataj.sh");
  printf("Usuwanie starych plikow!\n");
  if(argc!=3)
  {
    printf("nieprawidlowa ilosc argumentow programu!\n");
    exit(3);
  }
  iloscProducentow = atoi(argv[1]);
  iloscKonsumentow = atoi(argv[2]);
 if(iloscProducentow <0||iloscKonsumentow<0)
  {
    printf("zle argumenty wywolania\n");
    exit(4);
  }
  if(iloscProducentow+iloscKonsumentow>=150)
  {
    printf("Proszę podac mniejsza ilosc procesow\n");
    exit(5);
  }

  //czesc wlasciwa programu
  if(iloscProducentow!=0){
  utworzFifo ();
  }

  utworzProducenta (iloscProducentow);
  utworzKonsumenta (iloscKonsumentow);

  for(i=0; i<iloscProducentow+iloscKonsumentow; i++)
  {
    cpid = wait(&status);
    if(cpid == -1)
    {
      printf("Blad, potomka kod powrotu potomka to: %d\n", status/256);
    }  
  } 
  if(iloscProducentow!=0){
  printf("koniec kolejki fifo program unlink fifo!\n");
  unlink(path);}
  printf("\t WYNIK:\n");
  system("./zlicz.sh");
  
  return 0;
}
