#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
char nazwaFifo[] = "/tmp/Z2";
char nazwaPlikow[128];
int liczbaZnakow = 5;
void generateFile()
{
  FILE* txt;
  int i;
  char c;
  
  sprintf(nazwaPlikow,"producent/%ld",(long int)getpid());

  txt = fopen(nazwaPlikow,"w");
  if(!txt)
  {
    printf("PRODUCENT: Błąd otwarcia pliku!\n");
    exit(1);
  }

  for(i=0;i<liczbaZnakow;i++)
  {
    c = (rand()%70) + 32;
    fprintf(txt,"%c",c);
  }

  fclose(txt);
}

void putIntoFifo()
{
  int fifo;
  int n;
  FILE* fip;
  char c;
  int i;

  fip = fopen(nazwaPlikow,"r");
  if(!fip)
  {
    printf("PRODUCENT: Błąd otwarcia pliku z danymi!\n");
    exit(1);
  }

  printf("PRODUCENT: uzyskano dostep do pliku z danymi\n");

  while(1)
  {
    fifo = open(nazwaFifo,O_WRONLY);
    if(fifo == -1)
    {
      if(errno == ENXIO)
      {
        printf("PRODUCENT: ENXIO Czekam az ktos po drugiej stronie otworzy fifo do czytania\n");
        continue;
      }

      if(errno == EPIPE)
      {
        printf("PRODUCENT: EPIPE Czekam az ktos po drugiej stronie otworzy fifo do czytania\n");
        continue;
      }

      printf("PRODUCENT: nie mozna otworzyc pliku fifo\n");
      exit(1);
    }

    break;
  }

  while(1)
  { 
    
    sleep(1); // do poprawnej obslugi duzej ilosci konsumentow i producentow
    fscanf(fip,"%c",&c);
    if(feof(fip))
      break;

    n = write(fifo,&c,sizeof(char));
    if(n == -1)
    {
      if(errno == EPIPE)
      {
        printf("PRODUCENT: error\n");
        exit(1);
      }

      printf("PRODUCENT: Blad pisania do pliku fifo!\n");
      exit(1);
    }

    if(n == 0)
    {
      printf("PRODUCENT: Nie udalo sie nic wpisac do fifo\n");
    }
  }

  printf("PRODUCENT: skonczylem produkowac %d \n",getpid());

  close(fifo);
  fclose(fip);

}

int main()
{
  srand(getpid());
  generateFile();
  
  putIntoFifo();


  return 0;
}
