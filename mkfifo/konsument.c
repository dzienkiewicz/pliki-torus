#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>

char nazwaFifo[] = "/tmp/Z2";
char nazwaPlikow[128];
FILE* txt;

void zakonczProgram(int signal);

void start()
{
    signal (SIGINT, zakonczProgram);
  sprintf(nazwaPlikow,"konsument/%ld",(long int)getpid());
}

void koniec()
{
  if(txt!=NULL)
  {
    fclose(txt);
  }
}

void zakonczProgram(int signal)
{
  printf("\nKONSUMENT: Ctrl+C \n");
    koniec ();
  exit(0);
}

void getData()
{
  char c;
  int przeczytaneZnaki;
  int i,fifo;

  printf("KONSUMENT: Czekam az ktos po drugiej stronie otworzy fifo do pisania\n");
  fifo = open(nazwaFifo,O_RDONLY);
  if(fifo == -1)
  {

    printf("KONSUMENT: Błąd otwarcia pliku fifo!\n");
    exit(1);
  }
sleep(1);
  while(1)
  {
    if ((przeczytaneZnaki = read(fifo,&c,sizeof(char))) == -1)
    {
      printf("KONSUMENT: blad czytania fifo\n");
      exit(1);
    }

    if(przeczytaneZnaki == 0)
    {
      close(fifo);
      printf("KONSUMENT: koniec fifo\n");
      exit(0);
    }

    txt = fopen(nazwaPlikow,"a");
    if(!txt)
    {
      printf("KONSUMENT: Błąd otwarcia pliku wynikowego!\n");
      exit(1);
    }

    for(i=0;i<przeczytaneZnaki;i++)
    {
	printf("KONSUMENT PRZECZYTANY ZNAK %c",c);
      putc(c,txt);
    }

    fclose(txt);
  }
}

int main()
{
    start ();
  
    getData();

    koniec ();

  return 0;
}
