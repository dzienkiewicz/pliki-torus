#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>

int deskryptory[2];
int dlugoscPliku= 5;
void konsument();
void producent();
void stworzPlik();

void utworzProducenta(int n)
{
  pid_t id;
	int i;
  for(i=0; i<n; i++)
  {
    id = fork();
    if(id == -1)
    {
      printf("PRODUCENT: fork error\n");
      exit(1);
    }
    if(id == 0)
    {
        producent ();
      exit(0);
    }
  }
}

void utworzKonsumenta(int n)
{
  pid_t id;
	int i;
  for(i=0; i<n; i++)
  {
    id = fork();
    if(id == -1)
    {
      printf("KONSUMENT: Nieprawidlowe wywolanie fork()\n");
      exit(1);
    }
    if(id == 0)
    {
        konsument ();
      exit(0);
    }
  }
}

void konsument()
{
  char filename[32];
  char c;
  FILE* fop;

  close(deskryptory[1]);//zamykam pisanie

  sprintf(filename,"konsument/%ld",(long int)getpid());

  fop = fopen(filename,"w");
  if(!fop)
  {
    printf("KONSUMENT: Błąd otwarcia pliku wynikowego\n");
    exit(1);
  }

  while(read(deskryptory[0],&c,sizeof(char)))
    putc(c,fop);
  
  close(deskryptory[0]);

}

void producent()
{
  char filename[32];
  char c;
  FILE* fip;

  close(deskryptory[0]);//zamykam czytanie

  stworzPlik(filename);

  fip = fopen(filename,"r");
  if(!fip)
  {
    printf("PRODUCENT: Błąd otwarcia pliku z danymi!\n");
    exit(1);
  }

  while((c = getc(fip))!=EOF)
    write(deskryptory[1],&c,sizeof(char));
  
  close(deskryptory[1]);
}

void stworzPlik(char* filename)
{
  FILE* txt;
  char c;

  srand((unsigned int)getpid());

  sprintf(filename,"producent/%ld",(long int)getpid());

  txt = fopen(filename,"w");
  if(!txt)
  {
    printf("Błąd otwarcia pliku!\n");
    exit(1);
  }
	int i;
  for(i=0;i<dlugoscPliku;i++)
  {
    c = 'a';
    fprintf(txt,"%c",c);
  }

  fclose(txt);
}

int main(int argc, char* argv[])
{
  int i,cpid,status,iloscProducentow,iloscKonsumentow;
  system("./czysc.sh");
  printf("Usuwanie starych plikow!\n");
  if(argc!=3)
  {
    printf("nieprawidlowa ilosc argumentow programu!\n");
    exit(3);
  }
  iloscProducentow = atoi(argv[1]);
  iloscKonsumentow = atoi(argv[2]);
 if(iloscProducentow <0||iloscKonsumentow<0)
  {
    printf("zle argumenty wywolania\n");
    exit(4);
  }
  if(iloscProducentow+iloscKonsumentow>=150)
  {
    printf("Proszę podac mniejsza ilosc procesow\n");
    exit(5);
  }
  //czesc wlasciwa programu

  pipe(deskryptory);

    utworzProducenta (iloscProducentow);
    utworzKonsumenta (iloscKonsumentow);

  close(deskryptory[0]);
  close(deskryptory[1]);
  

  for(i=0; i<iloscProducentow+iloscKonsumentow; i++)
  {
    cpid = wait(&status);
    if(cpid == -1)
    {
      printf("Blad potomka kod powrotu potomka to: %d\n", status);
    }   
  } 
  printf("\t WYNIK:\n");
  system("./zlicz.sh");


  return 0;
}
